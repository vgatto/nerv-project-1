/**
 * Copyright Digital Engagement Xperience 2014
 * Created by Andrew
 *
 */

var HttpCodeError = require('./util/http-code-error');
var logger = require('./util/logger');
var oauth = require('oauth');

/**
 * Constructor for abstract channel handlers.
 * This class should be extended as necessary by concrete handler implementations.
 * @constructor
 */
function AbstractChannelHandler() {
}

function buildLogMsg(action, msg) {
    return "resource: abstract channel handler, action: " + action + ", " + msg;
}

/**
 * Determine whether a channel instance is compatible with the handler.
 *
 * @param {Object} channel channel instance to test
 * @return {Boolean} true if accepted, false otherwise
 */
AbstractChannelHandler.prototype.accept = function (channel) {
    logger.info(buildLogMsg("accept", "msg: returning true"));
    return true;

    //if((channel.url).indexOf("tumblr") > -1){
	//	console.log("Returning True!");
    //}else {
	//	console.log("Returning False!");
	//	return false;
  	//}

};
/**
 * Deliver a content playlist to a channel instance.
 * Result should be an array of objects corresponding to the posted SC instances.
 *
 * Result objects must at a minimum consist of { scObjectId: '...' } and should be
 * extended with any other data necessary to uniquely reference the deployed content
 * (e.g. post ID).
 *
 * @param {Object} params delivery parameters
 * @param {Object} playlist content playlist
 * @param {Function} callback invoked as callback([error], [result]) when finished
 */

var oa = new oauth.OAuth("https://www.tumblr.com/oauth/request_token",
    "https://www.tumblr.com/oauth/access_token",
    "crfsT65KG53kwQwjhS6CXwv3mt8UepUhTbUhP90MwiTXghvVxk",
   	"yl1u8EZx4OXQ3Qog9wi1AEhBlwkKNDiCGwVogqONjYH6tMvgd6",
    "1.0A",
    "http://nerviteration1.herokuapp.com/myhandler/callback",
    "HMAC-SHA1");

var tumblrOauthAccessToken;
var tumblrOauthAccessTokenSecret;

AbstractChannelHandler.prototype.deliver = function (params, playlist, oauthAT, oauthATS, callback) {

	tumblrOauthAccessToken = oauthAT;
	tumblrOauthAccessTokenSecret = oauthATS;

	console.log("In Abstract Channel Handler - Deliver Function");
	console.log("----------- Playlist ----------");
	//console.log(JSON.stringify(playlist[0].multimedia.text[0].property));
	//console.log("Playlist Name: "+playlist[0].multimedia.text[0].property.name);
	//console.log("Playlist Content: "+playlist[0].multimedia.text[0].property.content);
	
	//util.inspect(playlist,{showHidden:true});

	// Build the Tumblr request URL
	var post_port = 80;
	var post_path = '/v2/blog/therealestnerv.tumblr.com/post';
	var postUrl = 'http://api.tumblr.com' + post_path;
	// Set the post data, values come from the post request's body
	var post_data = {
		'type': "text",
		'state': "published",
		'title': playlist[0].scObj.property.name,
		'body': playlist[0].multimedia.text[0].property.content
	};
		
	// Create the post
	oa.post(postUrl, tumblrOauthAccessToken, tumblrOauthAccessTokenSecret, post_data, 'application/x-www-form-urlencoded', function(error, data, response) {
		if(error) return (response);
		var parsedData = JSON.parse(data);
		console.log(parsedData);
	});


    logger.info(buildLogMsg("deliver","msg: End of deliver"));
    callback(new HttpCodeError(501, 'deliver not implemented'));
};

/**
 * Get feedback (e.g. replies, comments) from previously delivered content.
 * Result should be an array of objects which use the format provided by
 * translateFeedback().
 *
 * @param {Object} params content parameters
 * @param {Function} callback invoked as callback([error], [result]) when finished
 */
AbstractChannelHandler.prototype.getFeedback = function (params, callback) {
    logger.info(buildLogMsg("getFeedback", "msg: not supported by this channel handler"));
    callback(new HttpCodeError(501, 'getFeedback not implemented'));
};

/**
 * Remove previously delivered content from the channel instance.
 *
 * The SC instance objects passed in will match those which were provided in the
 * response to the deliver() call.
 *
 * @param {Object[]} scInstances SC instances to be deleted
 * @param {Function} callback invoked as callback([error]) when finished
 */
AbstractChannelHandler.prototype.remove = function (scInstances, callback) {
    logger.info(buildLogMsg("remove", "msg: not supported by this channel handler"));
    callback(new HttpCodeError(501, 'remove not implemented'));
};


module.exports = AbstractChannelHandler;
