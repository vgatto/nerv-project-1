/**
 * Copyright Digital Engagement Xperience 2014
 * Created by Andrew
 *
 */
'use strict';

// third-party modules
var _ = require('underscore');
// internal modules
var logger = require('../lib/util/logger');
var oauth = require('oauth');
var request = require('request');



/**
 * Add routes for a third-party channel handler to an Express application.
 *
 * @param app express application
 * @param {AbstractChannelHandler} handler handler for processing third-party channel requests
 */
module.exports = function (app, handler) {

    if (!handler) {
        throw new Error('Service not found!');
    }

	// Create an OAuth object, using the Node.js OAuth Library
	var oa = new oauth.OAuth("https://www.tumblr.com/oauth/request_token",
    	"https://www.tumblr.com/oauth/access_token",
        "crfsT65KG53kwQwjhS6CXwv3mt8UepUhTbUhP90MwiTXghvVxk",
    	"yl1u8EZx4OXQ3Qog9wi1AEhBlwkKNDiCGwVogqONjYH6tMvgd6",
        "1.0A",
        "http://nerviteration1.herokuapp.com/myhandler/callback",
        "HMAC-SHA1");

	var oauthRequestToken;
	var oauthRequestTokenSecret;
   	var tumblrOauthAccessToken;
    var tumblrOauthAccessTokenSecret;



	app.get('/request', function (req, res) {
	    oa.getOAuthRequestToken(function(error, oauthToken, oauthTokenSecret){
	        if (error) {
	            res.send("Error getting OAuth request token: " + error, 500);
	        } else {
   	        	oauthRequestToken = oauthToken,
   	        	oauthRequestTokenSecret = oauthTokenSecret;
	 
   	    	    res.redirect("http://www.tumblr.com/oauth/authorize?oauth_token=" + oauthRequestToken);
   		     }
   		 });
	});



    app.post('/accept', function (req, res) {
		console.log("In Route Handler - Accept Function");
        logger.debug("accept:", req.body);
        var accepted = handler.accept(req.body);
        logger.debug("accepted:", accepted);
        res.status(200).send({accepted: accepted});
    });

    app.post('/deliver', function(req, res, next) {
		console.log("Deliver Call");
        handler.deliver(req.body.params, req.body.playlist, tumblrOauthAccessToken, tumblrOauthAccessTokenSecret, function(err, result) {
            if (err) {
                next(err);
            } else {
                logger.debug("delivered:", result);
                res.status(200).send(result);
            }
        });
    });

    app.post('/feedback', function(req, res, next) {
        handler.getFeedback(req.body, function(err, feedback) {
            if (err) {
                next(err);
            } else {
                logger.debug("feedback:", feedback);
                res.status(200).send(feedback);
            }
        });
    });

    app.post('/remove', function(req, res, next) {
        logger.debug("remove: ", req.body);
        handler.remove(req.body, function(err) {
            if (err) {
                next(err);
            } else {
                res.status(200).send();
            }
        });
    });

	app.get('/callback', function(req, res){  
		console.log('callin back');
    	oa.getOAuthAccessToken(oauthRequestToken, oauthRequestTokenSecret, req.query.oauth_verifier, function(error, _oauthAccessToken, _oauthAccessTokenSecret) {
    	    if (error) {
    	        res.send("Error getting OAuth access token: " + error, 500);
    	    } else {
    	        tumblrOauthAccessToken = _oauthAccessToken;
    	        tumblrOauthAccessTokenSecret = _oauthAccessTokenSecret;
				console.log('You are signed in - Access Token is : '+_oauthAccessToken);
				res.send("Authorization Successful!");
			}
   		});
	});


    return app;
};
